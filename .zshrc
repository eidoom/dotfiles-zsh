# Must be set before loading Prezto
POWERLEVEL9K_MODE='awesome-fontconfig'

# Source Prezto.
if [[ -s "${ZDOTDIR:-$HOME}/.zprezto/init.zsh" ]]; then
  source "${ZDOTDIR:-$HOME}/.zprezto/init.zsh"
fi

setopt extendedglob
unalias rsync

# fasd
# Prezto fasd module gives j for directory search
alias v='fasd -sif -e nvim' # quick opening files with nvim
alias f='fasd -sif -e xdg-open' # quick opening files with xdg-open

# don't add duplicates or commands starting with space to history
export HISTCONTROL=ignoredups:ignorespace

# Set default editor
EDITOR=nvim
VISUAL=nvim
alias vi=nvim
alias vvi='nvim -u NONE'

# Aliases
alias rr="source $HOME/.zshrc"
alias pd="cd -"
alias l="exa --group-directories-first"
alias ll="l -l"
alias up="sudo dnf update -y && sudo flatpak update -y && sudo snap refresh"
alias pipup="pip3 list --outdated --format=freeze | grep -v '^\-e' | cut -d = -f 1 | xargs -n1 pip3 install --upgrade --user"
alias cargoup="cargo install-update -a"
alias npmup="sudo npm install -g npm && sudo npm update -g"
#alias grep=egrep
alias mmv='noglob zmv -W'
alias dnfp='dnf provides'
alias gpl='git pull'
unalias gcl

# Suffix aliases
alias -s pdf=evince
alias -s djvu=evince
alias -s nb=mathematica
alias -s wl=mathematica
alias -s m=mathematica
alias -s md=nvim
alias -s tex=nvim
alias -s png=eog
alias -s jpg=eog
alias -s jpeg=eog

TERM="xterm-256color"

# powerlevel10k styling
POWERLEVEL9K_LEFT_PROMPT_ELEMENTS=(dir)
POWERLEVEL9K_RIGHT_PROMPT_ELEMENTS=(vcs background_jobs)

POWERLEVEL9K_SHORTEN_STRATEGY="truncate_from_right"
POWERLEVEL9K_SHORTEN_DELIMITER=""
POWERLEVEL9K_SHORTEN_DIR_LENGTH=1
POWERLEVEL9K_DIR_SHOW_WRITABLE=true

POWERLEVEL9K_DIR_WRITABLE_FORBIDDEN_BACKGROUND="black"
POWERLEVEL9K_DIR_WRITABLE_FORBIDDEN_FOREGROUND="red"
POWERLEVEL9K_DIR_HOME_BACKGROUND="black"
POWERLEVEL9K_DIR_HOME_FOREGROUND="blue"
POWERLEVEL9K_DIR_HOME_SUBFOLDER_BACKGROUND="black"
POWERLEVEL9K_DIR_HOME_SUBFOLDER_FOREGROUND="blue"
POWERLEVEL9K_DIR_DEFAULT_BACKGROUND="black"
POWERLEVEL9K_DIR_DEFAULT_FOREGROUND="blue"

POWERLEVEL9K_VCS_CLEAN_BACKGROUND="black"
POWERLEVEL9K_VCS_CLEAN_FOREGROUND="green"
POWERLEVEL9K_VCS_MODIFIED_BACKGROUND="black"
POWERLEVEL9K_VCS_MODIFIED_FOREGROUND="yellow"
POWERLEVEL9K_VCS_UNTRACKED_BACKGROUND="black"
POWERLEVEL9K_VCS_UNTRACKED_FOREGROUND="yellow"

source $HOME/.zshrc-private
