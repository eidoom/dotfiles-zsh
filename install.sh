#!/usr/bin/env bash

here=$(realpath "$0" | xargs dirname)
cd "$here" || exit

echo "# Installing system pacakges"
if [[ $(grep PRETTY_NAME /etc/os-release | sed 's/PRETTY_NAME="\(\w\+\).*"/\1/g') == Fedora ]]; then
    sudo dnf install -y zsh util-linux-user fontawesome-fonts exa bat ripgrep colordiff wdiff pydf htop yp-tools unar p7zip
elif [[ $(grep PRETTY_NAME /etc/os-release | sed 's/PRETTY_NAME="\(\w\+\).*"/\1/g') == Debian ]]; then
    sudo apt install -y zsh exa ripgrep colordiff wdiff pydf htop unar p7zip
fi

log=install.log
touch "$log"

echo "# Linking files"
for file in .??*; do
    if [[ "$file" != *".git"* && "$file" != *".swp" ]]; then
        if [[ -d "$file" ]]; then
            echo "Linking directory $file"
            link="$HOME/$file"
            ln -s "$here/$file" ~
            if grep -Fxq "$link" "$log"; then
                echo "$link previously installed"
            else
                echo "Logging installation of symlink $link to $log"
                echo "$link" >>"$log"
            fi
        else
            echo "Linking file $file"
            link="$HOME/$file"
            ln -s "$here/$file" "$link"
            if grep -Fxq "$link" "$log"; then
                echo "$link previously installed"
            else
                echo "Logging installation of symlink $link to $log"
                echo "$link" >>"$log"
            fi
        fi
    fi
done

echo "# Linking runcoms"
runcoms=( zlogin zlogout zprofile zshenv )
for runcom in ${runcoms[@]}; do
    link="$HOME/.$runcom"
    ln -s "$here"/.zprezto/runcoms/"$runcom" "$link"
            if grep -Fxq "$link" "$log"; then
                echo "$link previously installed"
            else
                echo "Logging installation of symlink $link to $log"
                echo "$link" >>"$log"
            fi
done

touch $HOME/.zshrc-private

echo "# Setting shell"
ZSH=/bin/zsh
if [[ $SHELL != "$ZSH" ]]; then
    chsh -s "$ZSH"
fi
