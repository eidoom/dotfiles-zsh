# [dotfiles-zsh](https://gitlab.com/eidoom/dotfiles-zsh)

## Description

My installation script and configuration files for `zsh`.
Tested with Fedora 31 and Debian 10.
Uses: 
* [Prezto](https://github.com/sorin-ionescu/prezto) `zsh` configuration framework.
* [Powerlevel10k](https://github.com/romkatv/powerlevel10k) `zsh` theme.

## Usage

### Manual installation

* Install with 
    * SSH clone (if you're me)
    ```shell
    git clone --recurse-submodules -j8 git@gitlab.com:eidoom/dotfiles-zsh.git
    ```
    * or HTTPS clone (if you're not me)
    ```shell
    git clone --depth 1 --recurse-submodules -j8 https://gitlab.com/eidoom/dotfiles-zsh.git
    ```
* then run installation script
```shell
cd dotfiles-zsh
./install.sh
```
* then log out and in again to effect the change of shell to `zsh`.

### Superproject

* Alternatively, install as part of the [superproject](https://gitlab.com/eidoom/dotfiles-public).

### Customisation

* Add machine-specific customisations to `~/.zshrc-private`.

## Feature highlight
### [Fasd](https://github.com/clvv/fasd)
* Uses Prezto [fasd module](https://github.com/sorin-ionescu/prezto/tree/master/modules/fasd)
* Jump [frecent](https://developer.mozilla.org/en-US/docs/Mozilla/Tech/Places/Frecency_algorithm) directories with `j <search>`
* Go back `<number>` directories with `<number>`
* Open frecent general files with `f <search>`
* Open frecent code in `nvim` with `v <search>`
### Git aliases
* Prezto [git module](https://github.com/sorin-ionescu/prezto/tree/master/modules/git)
### DNF aliases
* Prezto [dnf module](https://github.com/sorin-ionescu/prezto/tree/master/modules/dnf)
### History substring search
* Prezto [history-substring-search module](https://github.com/sorin-ionescu/prezto/tree/master/modules/history-substring-search) for [zsh-history-substring-search](https://github.com/zsh-users/zsh-history-substring-search)
### Autosuggestions
* Prezto [autosuggestions module](https://github.com/sorin-ionescu/prezto/tree/master/modules/autosuggestions) for [zsh-autosuggestions](https://github.com/zsh-users/zsh-autosuggestions)
### Aliases
* Open general files with `o`
* Open `nvim` with `vi`
* Repeat last command with `r`
* Reload configuration with `rr`
* `tmuxl` for `tmux list-sessions`
* `tmuxa` for `tmux  new-session -A`
### Utilities
* `l` for pretty `ls` with [exa](https://github.com/ogham/exa)
* `rg` for better `grep` with [ripgrep](https://github.com/BurntSushi/ripgrep)
* [`bat`](https://github.com/sharkdp/bat) for prettier `cat` (Fedora only)
* `wdiff` for better `diff`
* View processes with `htop`
* `pydf` colourises `df`
